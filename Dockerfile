FROM cschranz/gpu-jupyter:v1.4_cuda-11.0_ubuntu-20.04_python-only
USER root
RUN wget https://packages.vmware.com/bitfusion/ubuntu/20.04/bitfusion-client-ubuntu2004_4.5.0-4_amd64.deb && \
    apt-get update && apt-get install -y ./bitfusion-client-ubuntu2004_4.5.0-4_amd64.deb && \
    rm -rf ./bitfusion-client-ubuntu2004_4.5.0-4_amd64.deb
RUN fix-permissions /etc/bitfusion
RUN cp -r /opt/conda/share/jupyter/kernels/python3 /opt/conda/share/jupyter/kernels/bitfusion
COPY kernel.json /opt/conda/share/jupyter/kernels/bitfusion/
RUN rm -rf /etc/jupyter/jupyter_notebook_config.json
RUN conda install "nbconvert = 5.6.1"
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install -y build-essential unixodbc-dev && apt-get install -y libboost-locale-dev graphviz curl gnupg gnupg2 gnupg1
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && curl https://packages.microsoft.com/config/ubuntu/20.04/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update && \
    ACCEPT_EULA=Y apt-get install -y msodbcsql17 && \
    # optional: for bcp and sqlcmd
    ACCEPT_EULA=Y apt-get install -y mssql-tools && \
    echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc && \
    source ~/.bashrc
COPY ./dremio-odbc_1.5.3.1000-2_amd64.deb /tmp
RUN dpkg -i /tmp/dremio-odbc_1.5.3.1000-2_amd64.deb && rm /tmp/dremio-odbc_1.5.3.1000-2_amd64.deb
RUN fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER
USER ${NB_UID}

# Install from requirements.txt file
COPY --chown=${NB_UID}:${NB_GID} ./full-requirements.txt /tmp
RUN pip install --requirement /tmp/full-requirements.txt --no-cache-dir && \
    python -m spacy download en_core_web_trf && \
    python -m spacy download en_core_web_sm && \
    python -m nltk.downloader punkt wordnet stopwords && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

# Point to outpost.ws pypi mirror
COPY ./pip.conf /etc/
COPY --chown=${NB_UID}:${NB_GID} ./DenodoDialectSQLAlchemy.zip /tmp
RUN unzip /tmp/DenodoDialectSQLAlchemy.zip -d /opt/conda/lib/python3.8/site-packages/sqlalchemy/ && \
    mv /opt/conda/lib/python3.8/site-packages/sqlalchemy/denodo-sqlalchemy-dialect-8.0-20201204/denodo /opt/conda/lib/python3.8/site-packages/sqlalchemy/dialects/ && \
    rm -rf /opt/conda/lib/python3.8/site-packages/sqlalchemy/denodo-sqlalchemy-dialect-8.0-20201204 && \
    chattr -i -a /tmp/DenodoDialectSQLAlchemy.zip && \
    chmod ugo+w /tmp/DenodoDialectSQLAlchemy.zip && \
    rm /tmp/DenodoDialectSQLAlchemy.zip

USER root
RUN apt-get update && apt-get install -y tesseract-ocr poppler-utils && \
    apt-get clean && rm -rf /var/lib/apt/lists/*
USER ${NB_UID}
